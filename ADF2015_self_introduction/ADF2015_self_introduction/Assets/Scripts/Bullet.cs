﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	public float attackPower = 1;
	public GameObject attackEffect;

	void OnTriggerEnter(Collider collision) {
		if (collision.gameObject.tag == "Enemy") {
			//Destroy(collision.gameObject);
			collision.gameObject.SendMessage("Hit", attackPower);
			Destroy(Instantiate(attackEffect, transform.position, transform.rotation), 2.0f);
		}
		if(collider.gameObject.tag == "Enemy" || collider.gameObject.tag == "Untagged") {
			Destroy(gameObject);
		}
	}
}
