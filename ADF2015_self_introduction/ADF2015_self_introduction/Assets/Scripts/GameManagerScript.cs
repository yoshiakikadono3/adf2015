﻿using UnityEngine;
using System.Collections;

public class GameManagerScript : MonoBehaviour
{

		enum State
		{
				ONGAME,
				PAUSE,
				CLEAR,
				GAMEOVER,
				WARNING}
		;
		int GameState;
		int previousState;

		// Use this for initialization
		void Start ()
		{
				GameState = (int)State.ONGAME;
				previousState = GameState;
		}
	
		// Update is called once per frame
		void Update ()
		{

				if (Input.GetKeyDown (KeyCode.P) && GameState == (int)State.ONGAME) {
						Application.LoadLevelAdditive ("Pause");
						GameState = (int)State.PAUSE;
				}
				if (GameState == (int)State.CLEAR && previousState == (int)State.ONGAME) {
						Invoke("LoadLevelClear", 2);
						//Application.LoadLevelAdditive ("Clear");
				}

				if (GameState == (int)State.GAMEOVER && previousState == (int)State.ONGAME) {
						Application.LoadLevelAdditive ("GameOver");
				}

				if (GameState == (int)State.WARNING && previousState == (int)State.ONGAME) {
						Application.LoadLevelAdditive ("Warning");
						GameState = (int)State.WARNING;
				}
				previousState = GameState;

		}
		
		void LoadLevelClear() {
			Application.LoadLevelAdditive ("Clear");
		}

		void SetGameState (string stateStr)
		{
				if (stateStr.Equals ("Clear")) {
						GameState = (int)State.CLEAR;
				}
				if (stateStr.Equals ("Pause")) {
						GameState = (int)State.PAUSE;
				}
				if (stateStr.Equals ("OnGame")) {
						GameState = (int)State.ONGAME;
				}
				if (stateStr.Equals ("GameOver")) {
						GameState = (int)State.GAMEOVER;
				}
				if (stateStr.Equals ("Warning")) {
						GameState = (int)State.WARNING;
				}

		}

}
