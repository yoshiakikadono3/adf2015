﻿using UnityEngine;
using System.Collections;

public class WarningTexture : MonoBehaviour {
	private GUITexture background;
	// Use this for initialization
	void Start () {
		background = this.guiTexture;
		background.pixelInset = new Rect (-1 * Screen.width / 2, -1 * Screen.height / 2 + Screen.height/4, Screen.width, Screen.height/2);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
