﻿using UnityEngine;
using System.Collections;

public class PauseScript : MonoBehaviour
{
		private GUIStyle style;
		private GUIStyleState styleState;

		// Use this for initialization
		void Start ()
		{
				style = new GUIStyle ();
				styleState = new GUIStyleState ();
				style.fontSize = (int)(Screen.width / 10);
				styleState.textColor = Color.black;
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.P)) {
						Destroy (this.gameObject);
				}
				
		}

		void Awake ()
		{
				GameObject gameObj = GameObject.Find ("Main Camera");
				//gameObj.GetComponent<PlayerController> ().enabled = false;
				gameObj.GetComponent<MouseLook> ().enabled = false;
				gameObj.GetComponent<Gun> ().enabled = false;

				GameObject enemyObj = GameObject.FindGameObjectWithTag ("EnemyParent");
				enemyObj.GetComponent<EnemyMove> ().enabled = false;

				GameObject player = GameObject.Find ("Robot Kyle");
				player.GetComponent<Player> ().enabled = false;


				Screen.showCursor = true;
				Screen.lockCursor = false;
				Time.timeScale = 0;
		}

		void OnDestroy ()
		{
				GameObject gameObj = GameObject.Find ("Main Camera");
				//gameObj.GetComponent<PlayerController> ().enabled = true;
				gameObj.GetComponent<MouseLook> ().enabled = true;
				gameObj.GetComponent<Gun> ().enabled = true;
		
				GameObject enemyObj = GameObject.FindGameObjectWithTag ("EnemyParent");
				enemyObj.GetComponent<EnemyMove> ().enabled = true;

				GameObject player = GameObject.Find ("Robot Kyle");
				player.GetComponent<Player> ().enabled =  true;

				Time.timeScale = 1;
				GameObject.Find ("GameManager").SendMessage ("SetGameState", "OnGame");

		}

		void OnGUI ()
		{
				string str = "Pause";
				style.normal = styleState;
				Vector2 strSize = style.CalcSize (new GUIContent (str));
				GUI.Label (new Rect (Screen.width / 2 - strSize.x / 2, Screen.height / 2 - strSize.y / 2, strSize.x, strSize.y), str, style);
				if (GUI.Button (new Rect (Screen.width / 2 - 50, Screen.height - 40, 100, 30), "もどる")) {
						Destroy (this.gameObject);
				}
		}
}
