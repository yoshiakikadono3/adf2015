﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public float speed = 6.0f;
	public float jumpSpeed = 8.0f;
	public float gravity = 20.0f;
	
	private Vector3 moveDirection = Vector3.zero;
	private CharacterController controller;
	private bool isPlaying;
	// Use this for initialization
	void Start () {
		controller = GetComponent<CharacterController>();
		isPlaying = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(!isAnyMoveKey() && controller.isGrounded) {
			return;
		}
	
		if (controller.isGrounded) {
		
			moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			moveDirection = transform.TransformDirection(moveDirection);
			moveDirection.y = 0;
			moveDirection = Vector3.Normalize(moveDirection) * speed;
			if(!isPlaying && !moveDirection.Equals(Vector3.zero)) {
				StartCoroutine("playFootSound");
				isPlaying = true;
			}
			if (Input.GetButton ("Jump")) {
				moveDirection.y = jumpSpeed;
			}
		}
		
		moveDirection.y -= gravity * Time.deltaTime;
		
		controller.Move(moveDirection * Time.deltaTime);
	}
	
	private IEnumerator playFootSound() {
		yield return new WaitForSeconds(0.33f);
		GameObject.Find("FootSound").SendMessage("playSound");
		isPlaying = false;
	}
	
	private bool isAnyMoveKey() {
		if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D) || Input.GetButton ("Jump")) {
			return true;
		} else {
			return false;
		}
	}
}
