﻿using UnityEngine;
using System.Collections;

public class BackgroundTextureScript : MonoBehaviour {
	private GUITexture background;
	// Use this for initialization
	void Start () {
		background = this.guiTexture;
		background.pixelInset = new Rect (-1 * Screen.width / 2, -1 * Screen.height / 2, Screen.width, Screen.height);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

}
