﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
		public float HP = 100;
		public float knockback = 0.5f;
		private float amount;
		private CharacterController cc;
		private Vector3 direction;
		// Use this for initialization
		void Start ()
		{
				amount = 0.0f;
				cc = transform.parent.gameObject.GetComponent<CharacterController> ();
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (amount > 0) {
						cc.Move (direction * amount);
						amount -= Time.deltaTime;
				}
		}
	
		void Hit (Transform target)
		{
				amount = knockback;
				direction = Vector3.Normalize (this.transform.position - target.transform.position);
		}

		void ApplyDamage (float damage)
		{
				HP -= damage;
				Debug.Log (HP);
				if (HP < 0 || HP == 0 || Input.GetKeyDown (KeyCode.K)) {
						HP = 0;
						GameObject.Find ("HPBar").SendMessage ("SetHP", HP);
						GameObject.Find ("GameManager").SendMessage ("SetGameState", "GameOver");
						
				} else {
						GameObject.Find ("HPBar").SendMessage ("SetHP", HP);
				}
		}
}
