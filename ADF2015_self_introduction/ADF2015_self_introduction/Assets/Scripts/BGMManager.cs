﻿using UnityEngine;
using System.Collections;

public class BGMManager : MonoBehaviour {
	public GameObject baseBGM;
	public GameObject bossBGM;
	
	private GameObject basebgm;
	private GameObject bossbgm;
	// Use this for initialization
	void Start () {
		basebgm = (GameObject) Instantiate(baseBGM, Vector3.zero, Quaternion.Euler(Vector3.zero));
		bossbgm = null;
	}
	
	void appearBoss() {
		if(basebgm != null) basebgm.SetActive(false);
		if(bossbgm == null) {
			bossbgm = (GameObject) Instantiate(bossBGM, Vector3.zero, Quaternion.Euler(Vector3.zero));
		}
		bossbgm.SetActive(true);
	}
	
	void disappearBoss() {
		if(bossbgm != null) bossbgm.SetActive(false);
		if(basebgm == null) {
			basebgm = (GameObject) Instantiate(baseBGM, Vector3.zero, Quaternion.Euler(Vector3.zero));
		}
		basebgm.SetActive(true);
	}
}
