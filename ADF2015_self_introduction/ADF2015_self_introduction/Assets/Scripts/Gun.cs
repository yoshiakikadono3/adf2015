﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {
	public float initialVelocity = 50;
	public GameObject bulletPrefab;
	private AudioSource shotSound;
	
	public int maxBullets;
	public float reloadInterval;
	public float shootInterval;

	private int nowBullets;
	private bool  isReload;
	private float reloadTimer;
	private float shootTimer;

	// Use this for initialization
	void Start () {
		nowBullets = maxBullets;
		isReload = false;
		shootTimer = 0.0f;
		shotSound = this.audio;
	}
	
	// Update is called once per frame
	void Update () {
		shootTimer -= Time.deltaTime;
		if(Input.GetMouseButton(0) && nowBullets > 0  && !isReload && shootTimer <= 0.0f) {
			nowBullets--;
			shootTimer = shootInterval;

			Vector3 screenPoint = Input.mousePosition;
			screenPoint.z = 10f;
			
			Vector3 worldPoint = camera.ScreenToWorldPoint(screenPoint);
			Ray ray = camera.ScreenPointToRay(new Vector3(Screen.width/2, Screen.height/2, 0));
			//Vector3 direction = transform.forward;
			Vector3 direction = Vector3.Normalize(worldPoint - transform.position);
			//Vector3 direction = ray.direction;
//			Debug.Log (transform.position + ", " + worldPoint + ", " + ", " + direction);
			GameObject bullet = (GameObject)Instantiate (bulletPrefab, transform.position/* + 2*direction*/, transform.rotation);
			bullet.rigidbody.velocity = transform.forward * initialVelocity;
			shotSound.PlayOneShot(shotSound.clip, 0.5f);
		}
		
		if(Input.GetKeyDown(KeyCode.R) && !isReload  && nowBullets < maxBullets) {
			isReload = true; 
			reloadTimer = 0.0f;
		}

		if(isReload) {
			reloadTimer += Time.deltaTime;
			if(reloadTimer > reloadInterval) {
				isReload = false;
				reloadTimer = 0.0f;
				nowBullets = maxBullets; 
			}
		}

//		Event e = Event.current;
//		if(e != null) 
//			Debug.Log(e.mousePosition);

		 
	}
	/*
	void OnGUI() {
		int sw = Screen.width;
		int sh = Screen.height;
		
		GUI.Label (new Rect (0, sh/3 , sw, sh/2), "Bullets "  + nowBullets , "Message");

		if(isReload) {
			string text = Mathf.CeilToInt (3.0f - reloadTimer).ToString ();
			GUI.color  = new Color (1, 1, 1, reloadTimer - Mathf.FloorToInt (reloadTimer ));
			GUI.Label (new Rect (0, sh / 4, sw, sh / 2), text, "Message");
		}
	}
	*/
}
