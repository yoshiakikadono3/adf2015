﻿using UnityEngine;
using System.Collections;

public class Resporn : MonoBehaviour {
	
	void OnTriggerEnter(Collider col) {
		if(col.gameObject.tag == "MainCamera") {
			col.transform.position = new Vector3(0, 0.5f, 0);
		} else {
			Destroy(col.gameObject);
		}
	}
}
