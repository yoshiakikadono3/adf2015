﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	public float HP;
	public GameObject DestroyedEffect;
	public float attackPower = 1;
	public float attackInterval = 2;
	
	private float timer;
	// Use this for initialization
	void Start () {
		// Debug
		//StartCoroutine("timeDamage");
		timer = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		if(timer > 0) {
			timer -= Time.deltaTime;
		}
	}
//	IEnumerator timeDamage(){
//		for(;;){
//			this.sendMessage("ApplyDamage",1);
//			yield return new WaitForSeconds(2f);
//	
//		}
//	}
	void sendMessage(string getstring,float damage){
		if(getstring.Equals ("ApplyDamage")){
			HP -= damage;
		}
		if(HP < 0){
			//Object.Instantiate(DestroyedEffect);
			Object.Destroy (this.gameObject);
		}
	}
	
	void ApplyDamage(float damage) {
		HP -= damage;
		Debug.Log ("HP: " + HP);
		if(HP <= 0){
			Destroy(Object.Instantiate(DestroyedEffect, transform.position + Vector3.up/2, transform.rotation), 5.0f);
			GameObject.Find("EnemyGenerator").SendMessage("enemyDestroy");
			Object.Destroy (this.gameObject);
		}
	}
	
	void OnTriggerEnter(Collider collision) {
		if (collision.gameObject.tag == "Player" && timer <= 0) {
			collision.gameObject.SendMessage("Hit", transform);
			collision.gameObject.SendMessage("ApplyDamage", attackPower);
			timer = attackInterval;
		}
	}
}
