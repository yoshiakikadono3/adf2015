﻿using UnityEngine;
using System.Collections;

public class EnemyMove : MonoBehaviour {
	private GameObject target;
	private Vector3 vec;
	private Transform tgtPos;
	public float speed = 0.07f;
	public float recognitionRange = 35.0f;
	public float interval = 1;

	private float timer;
	private AudioSource footSound;
	// Use this for initialization
	void Start () {
		target = GameObject.FindWithTag("Player");
		timer = 0;
		footSound = this.audio;
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if((Vector3.Distance (target.transform.position , this.transform.position)) < recognitionRange){
			this.transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.LookRotation(target.transform.position - this.transform.position),0.07f);
			this.transform.rotation = Quaternion.Euler(new Vector3(0,this.transform.eulerAngles.y,0));
			 GameObject obj = GameObject.FindGameObjectWithTag("Player");
			//this.transform.position += this.transform.forward * speed;
			this.transform.position += Vector3.Normalize(obj.transform.position - this.transform.position) * speed * Time.deltaTime;
		}
		if(timer >= interval) {
			this.rigidbody.AddForce(Vector3.up * 300);
			footSound.PlayOneShot(footSound.clip);
			timer = 0;
		}
	}
	
	void OnTriggerStay(Collider col) {
		if(col.gameObject.tag != "Enemy" && col.gameObject.tag != "EnemyParent") {
			this.transform.position += (Vector3.up * Time.deltaTime * 3);
		}
	}
}