﻿using UnityEngine;
using System.Collections;

public class StartScript : MonoBehaviour
{

		// Use this for initialization
		void Start ()
		{
				Screen.showCursor = true;
				Screen.lockCursor = false;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		void OnGUI ()
		{
				if (GUI.Button (new Rect (Screen.width / 2 - 50, Screen.height - 100, 100, 30), "スタート")) {
						Application.LoadLevel ("CharacterMove");
				}
		}
}
