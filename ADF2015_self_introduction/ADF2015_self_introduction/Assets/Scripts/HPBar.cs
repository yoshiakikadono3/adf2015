﻿using UnityEngine;
using System.Collections;

public class HPBar : MonoBehaviour
{
		private GUITexture background;
		private int HP;
		// Use this for initialization
		void Start ()
		{
				HP = 100;
				background = this.guiTexture;
				background.pixelInset = new Rect (-1 * Screen.width / 3, -4 * Screen.height / 10, 6 * Screen.width / 10, 10);
		}
	
		// Update is called once per frame
		void Update ()
		{
				background.pixelInset = new Rect (-1 * Screen.width / 3, -4 * Screen.height / 10, HP * 6 * Screen.width / 1000, 10);
		}

		void OnGUI ()
		{
				background = this.guiTexture;
				background.pixelInset = new Rect (-1 * Screen.width / 3, -4 * Screen.height / 10, 6 * Screen.width / 10, 10);
				
		}

		void SetHP (int nowHP)
		{
				HP = nowHP;
		}
}
