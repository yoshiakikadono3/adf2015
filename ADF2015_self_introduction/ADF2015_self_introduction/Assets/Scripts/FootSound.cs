﻿using UnityEngine;
using System.Collections;

public class FootSound : MonoBehaviour {
	private AudioSource footSound;
	
	// Use this for initialization
	void Start () {
		footSound = this.audio;
	}
	
	void playSound() {
		footSound.PlayOneShot(footSound.clip);
	}
}
