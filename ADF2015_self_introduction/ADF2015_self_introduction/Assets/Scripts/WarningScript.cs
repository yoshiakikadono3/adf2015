﻿using UnityEngine;
using System.Collections;

public class WarningScript : MonoBehaviour
{
		private GUIStyle style;
		private GUIStyleState styleState;
		private float timer;
		private float waitingTime;
		// Use this for initialization
		void Start ()
		{
				style = new GUIStyle ();
				styleState = new GUIStyleState ();
				style.fontSize = (int)(Screen.width / 10);
				timer = 0;
				waitingTime = 2;
		}
	
		// Update is called once per frame
		void Update ()
		{
				timer += Time.deltaTime;
				if (timer > waitingTime) {
						Destroy (this.gameObject);
						timer = 0;
				}

		}

		void OnDestroy ()
		{
				GameObject.Find ("GameManager").SendMessage ("SetGameState", "OnGame");
		}

		void OnGUI ()
		{
				string str = "WARNING!!";
				Vector2 strSize = style.CalcSize (new GUIContent (str));
		
				styleState.textColor = Color.gray;
				style.normal = styleState;
				GUI.Label (new Rect (Screen.width / 2 - strSize.x / 2 + 2, Screen.height / 2 - strSize.y / 2 + 2, strSize.x, strSize.y), str, style);
		
				styleState.textColor = Color.yellow;
				style.normal = styleState;
				GUI.Label (new Rect (Screen.width / 2 - strSize.x / 2, Screen.height / 2 - strSize.y / 2, strSize.x, strSize.y), str, style);

		}
}
