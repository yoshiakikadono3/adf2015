﻿using UnityEngine;
using System.Collections;

public class BOSS : MonoBehaviour
{

	// Use this for initialization
	void Start () {
	
	}
	
		// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.J)) {
			GameObject.Find ("GameManager").SendMessage ("SetGameState", "Clear");
		} else if(Input.GetKey (KeyCode.K)) {
			GameObject.Find("BGMManager").SendMessage("appearBoss");
			GameObject.Find ("GameManager").SendMessage ("SetGameState", "Warning");
		} else if(Input.GetKey (KeyCode.L)) {
			GameObject.Find("BGMManager").SendMessage("disappearBoss");
		}
	}
}
