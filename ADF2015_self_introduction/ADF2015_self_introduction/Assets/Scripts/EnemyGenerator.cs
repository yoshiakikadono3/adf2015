﻿using UnityEngine;
using System.Collections;

public class EnemyGenerator : MonoBehaviour {
	public GameObject enemyPrefab;
	public int firstNumber;
	public int additionalNumber;
	public float interval;
	public float range = 25;
	public int bossNumber = 30;
	
	public int destroyCountDown;
	private GameObject player;
	private float timer;
	private bool boss;
	// Use this for initialization
	void Awake() {
		destroyCountDown = firstNumber + additionalNumber + bossNumber;
	}
	
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
		for(int i=0;i<firstNumber;i++) {
			Vector3 v = new Vector3(Random.Range(-range, range) ,transform.position.y ,Random.Range(-range, range));
			Instantiate(enemyPrefab, v + player.transform.position, transform.rotation);
		}
		timer = 0.0f;
		boss = false;
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if(timer > interval && additionalNumber > 0) {
			Vector3 v = new Vector3(Random.Range(-range, range) ,transform.position.y ,Random.Range(-range, range));
			Instantiate(enemyPrefab, v + player.transform.position, transform.rotation);
			timer = 0.0f;
			additionalNumber--;
		}
		/*
		if(boss && destroyCountDown <= 0) {
			GameObject.Find("GameManager").SendMessage("SetGameState", "Clear");
		}
		*/
		if(destroyCountDown <= 0) {
			GameObject.Find("GameManager").SendMessage("SetGameState", "Clear");
		}
	}
	
	void enemyDestroy() {
		destroyCountDown--;
		//GameObject.Find ("ShowEnemyCount").SendMessage ("SetCount", destroyCountDown);
		GameObject.Find ("ShowEnemyCount").SendMessage("UpdateName");
		/*
		if(destroyCountDown <= bossNumber && !boss) {
			interval = 1;
			additionalNumber += bossNumber;
			GameObject.Find("BGMManager").SendMessage("appearBoss");
			GameObject.Find("GameManager").SendMessage("SetGameState", "Warning");
			boss = true;
			range *= 2;
		}
		*/
	}
}
