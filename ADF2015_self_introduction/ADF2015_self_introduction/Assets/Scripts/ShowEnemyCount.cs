﻿using UnityEngine;
using System.Collections;

public class ShowEnemyCount : MonoBehaviour {

	private int enemyCount;
	private GUIText showEnemy;
	
	private string name = "KadonoYoshiaki";
	private int nameIndex = 0;
		
	// Use this for initialization
	void Start () {
		EnemyGenerator eg = GameObject.Find ("EnemyGenerator").GetComponent<EnemyGenerator> ();
			showEnemy = this.GetComponent<GUIText> ();
			showEnemy.text = "";
			//SetCount (eg.destroyCountDown);
			//SetCount (100);
			//sh owEnemy.text = enemyCount.ToString ();
	}
		
	void SetCount (int count) {
			enemyCount = count;
			showEnemy.text = enemyCount.ToString ();
	}
		
	void UpdateName() {
		if(nameIndex >= name.Length) return;
		showEnemy.text = name.Substring(0, ++nameIndex);
	}
			
}
