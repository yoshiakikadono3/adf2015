﻿using UnityEngine;
using System.Collections;

public class ExplosionSound : MonoBehaviour {
	private AudioSource destroySound;
	// Use this for initialization
	void Start () {
		destroySound = this.audio;
		destroySound.PlayOneShot(destroySound.clip);
	}
}
