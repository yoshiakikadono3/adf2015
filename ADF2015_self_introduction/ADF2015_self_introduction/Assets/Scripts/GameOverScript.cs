﻿using UnityEngine;
using System.Collections;

public class GameOverScript : MonoBehaviour
{
	
		private GUIStyle style;
		private GUIStyleState styleState;

		// Use this for initialization
		void Start ()
		{
				Screen.showCursor = true;
				Screen.lockCursor = false;
				style = new GUIStyle ();
				styleState = new GUIStyleState ();
				style.fontSize = (int)(Screen.width / 10);
				styleState.textColor = Color.red;
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		void Awake ()
		{
				GameObject gameObj = GameObject.Find ("Main Camera");
				//gameObj.GetComponent<PlayerController> ().enabled = false;
				gameObj.GetComponent<MouseLook> ().enabled = false;
				gameObj.GetComponent<Gun> ().enabled = false;
				GameObject player = GameObject.Find ("Robot Kyle");
				player.GetComponent<Player> ().enabled = false;
		
				GameObject enemyObj = GameObject.FindGameObjectWithTag ("EnemyParent");
				enemyObj.GetComponent<EnemyMove> ().enabled = false;
		
				Time.timeScale = 0;
		}
	
		void OnDestroy ()
		{
				GameObject gameObj = GameObject.Find ("Main Camera");
				//gameObj.GetComponent<PlayerController> ().enabled = true;
				gameObj.GetComponent<MouseLook> ().enabled = true;
				gameObj.GetComponent<Gun> ().enabled = true;
				GameObject player = GameObject.Find ("Robot Kyle");
				player.GetComponent<Player> ().enabled = true;


				GameObject enemyObj = GameObject.FindGameObjectWithTag ("EnemyParent");
				enemyObj.GetComponent<EnemyMove> ().enabled = true;
		
		
				Time.timeScale = 1;
				GameObject.Find ("GameManager").SendMessage ("SetGameState", "OnGame");
		
		}
	
		void OnGUI ()
		{
				string str = "GAME OVER";
				style.normal = styleState;
				Vector2 strSize = style.CalcSize (new GUIContent (str));
				GUI.Label (new Rect (Screen.width / 2 - strSize.x / 2, Screen.height / 2 - strSize.y / 2, strSize.x, strSize.y), str, style);
				if (GUI.Button (new Rect (Screen.width / 2 - 150, Screen.height - 40, 100, 30), "最初から")) {
						Destroy (this.gameObject);
						Application.LoadLevel ("CharacterMove");
				}
				if (GUI.Button (new Rect (Screen.width / 2 + 50, Screen.height - 40, 100, 30), "タイトルへ")) {
						Destroy (this.gameObject);
						Application.LoadLevel ("Start");
				}
		}
}
