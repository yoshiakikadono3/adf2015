﻿using UnityEngine;
using System.Collections;

public class EnemyPart : MonoBehaviour {
	public string call_message = "Hit";
	public float damage_rate = 1.0f;
	
	private float timer;
	// Use this for initialization
	void Start () {
		timer = 0;
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
	}
	void sendMessage(string getstring, float damage){
		this.transform.parent.gameObject.SendMessage (call_message,damage * damage_rate);
	}
	
	void Hit(float damage){
		if(timer <= 0) {
			this.transform.parent.gameObject.SendMessage ("ApplyDamage", damage * damage_rate);
			timer = 0.01f;
		}
	}
}
