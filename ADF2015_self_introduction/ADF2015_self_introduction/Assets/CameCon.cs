﻿using UnityEngine;
using System.Collections;

public class CameCon : MonoBehaviour
{
		public GameObject player;
		// Use this for initialization
		void Start ()
		{

		}
	
		// Update is called once per frame
		void Update ()
		{

				Vector3 vectorToCamera = transform.position - player.transform.position;
				vectorToCamera.y = 0;
				Vector3 xzVectorToCamera = Vector3.Normalize (vectorToCamera);
				vectorToCamera = transform.position - player.transform.position;
				vectorToCamera.y *= 0.1f;
				Vector3 wantedPosition = player.transform.position + xzVectorToCamera * 1.0F;
				wantedPosition.y = player.transform.position.y + 0.5F;
				//プレイヤーどの位置に置くか
				//transform.position = new Vector3 (player.transform.position.x, player.transform.position.y + 2, player.transform.position.z - 4);
				
		transform.position = Vector3.Lerp (transform.position, wantedPosition, 10f * Time.deltaTime);
				
				if (!Input.GetMouseButton (0)) {

						transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (-vectorToCamera), Time.deltaTime * 6f);
				}	
		}
}